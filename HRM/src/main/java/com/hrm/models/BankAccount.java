package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class BankAccount {
	@Id
	@GeneratedValue
	private Integer bankAccountID;
	private String accountHolderName;
	private String bankName;
	private String typeOfAccount;
	private String branchName;
	private Long bankAccountNumber;
	private String ifscNumber;
	
	
	@OneToOne(mappedBy="bankDetails")
	private Employee employee;
	
	public Integer getBankAccountID() {
		return bankAccountID;
	}
	public void setBankAccountID(Integer bankAccountID) {
		this.bankAccountID = bankAccountID;
	}
	public String getAccountHolderName() {
		return accountHolderName;
	}
	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getTypeOfAccount() {
		return typeOfAccount;
	}
	public void setTypeOfAccount(String typeOfAccount) {
		this.typeOfAccount = typeOfAccount;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public Long getBankAccountNumber() {
		return bankAccountNumber;
	}
	public void setBankAccountNumber(Long bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	public String getIfscNumber() {
		return ifscNumber;
	}
	public void setIfscNumber(String ifscNumber) {
		this.ifscNumber = ifscNumber;
	}
	
	
	@Override
	public String toString() {
		return "BankAccount [bankAccountID=" + bankAccountID + ", accountHolderName=" + accountHolderName
				+ ", bankName=" + bankName + ", typeOfAccount=" + typeOfAccount + ", branchName=" + branchName
				+ ", bankAccountNumber=" + bankAccountNumber + ", ifscNumber=" + ifscNumber + "]";
	}
	
	
	

}
