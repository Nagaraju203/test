package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ManagerApprails {
	@Id
	@GeneratedValue	
	private Integer managRatingID;;
	
	private Employee employeeID;
	
	private Department depatmentID;
	private Integer finalcialYear;
	
	private String performance;
	
	private Integer managerRating;
	
	private String comments;

	public Integer getManagRatingID() {
		return managRatingID;
	}

	public void setManagRatingID(Integer managRatingID) {
		this.managRatingID = managRatingID;
	}

	public Employee getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(Employee employeeID) {
		this.employeeID = employeeID;
	}

	public Department getDepatmentID() {
		return depatmentID;
	}

	public void setDepatmentID(Department depatmentID) {
		this.depatmentID = depatmentID;
	}

	public Integer getFinalcialYear() {
		return finalcialYear;
	}

	public void setFinalcialYear(Integer finalcialYear) {
		this.finalcialYear = finalcialYear;
	}

	public String getPerformance() {
		return performance;
	}

	public void setPerformance(String performance) {
		this.performance = performance;
	}

	public Integer getManagerRating() {
		return managerRating;
	}

	public void setManagerRating(Integer managerRating) {
		this.managerRating = managerRating;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	

}
