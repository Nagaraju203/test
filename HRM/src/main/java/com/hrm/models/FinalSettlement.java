package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class FinalSettlement {

	@Id
	@GeneratedValue
	private Integer finalSettlementID;
	
	private Double deductions;
	
	private Double finalSettlementAmount;
	
	@OneToOne
	@JoinColumn(name="empid_fk")
	private Employee employee;
	
	@OneToOne
	@JoinColumn(name="depid_fk")
	private Department department;
	
	@OneToOne(mappedBy="finalSettlement")
	private NoObjectionCerificate noc;

	public Integer getFinalSettlementID() {
		return finalSettlementID;
	}

	public void setFinalSettlementID(Integer finalSettlementID) {
		this.finalSettlementID = finalSettlementID;
	}

	public Double getDeductions() {
		return deductions;
	}

	public void setDeductions(Double deductions) {
		this.deductions = deductions;
	}

	public Double getFinalSettlementAmount() {
		return finalSettlementAmount;
	}

	public void setFinalSettlementAmount(Double finalSettlementAmount) {
		this.finalSettlementAmount = finalSettlementAmount;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public NoObjectionCerificate getNoc() {
		return noc;
	}

	public void setNoc(NoObjectionCerificate noc) {
		this.noc = noc;
	}
	
	
	
}
