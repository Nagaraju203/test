package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class PerformanceManagement {
	@Id
	@GeneratedValue
	private Integer pmid;
	
	private SelfApprailsal selfApprailsalID;
	
	private ManagerApprails managerAppraisalID;

	public Integer getPmid() {
		return pmid;
	}

	public void setPmid(Integer pmid) {
		this.pmid = pmid;
	}

	public SelfApprailsal getSelfApprailsalID() {
		return selfApprailsalID;
	}

	public void setSelfApprailsalID(SelfApprailsal selfApprailsalID) {
		this.selfApprailsalID = selfApprailsalID;
	}

	public ManagerApprails getManagerAppraisalID() {
		return managerAppraisalID;
	}

	public void setManagerAppraisalID(ManagerApprails managerAppraisalID) {
		this.managerAppraisalID = managerAppraisalID;
	}
	

}
