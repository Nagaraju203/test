package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class EmployeeDocumentationCheckList {
	
	@Id
	@GeneratedValue
	private Integer empDocumentsCheckListID;

	private String depatmentInfo;
	private String checkListClearance;
	
	@OneToOne
	@JoinColumn(name="empid_fk")
	private Employee employee;
	

	@OneToOne(mappedBy="empDocumCheckList")
	private NoObjectionCerificate noc;
	
	
	
	
		
	
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public NoObjectionCerificate getNoc() {
		return noc;
	}
	public void setNoc(NoObjectionCerificate noc) {
		this.noc = noc;
	}
	public Integer getEmpDocumentsCheckListID() {
		return empDocumentsCheckListID;
	}
	public void setEmpDocumentsCheckListID(Integer empDocumentsCheckListID) {
		this.empDocumentsCheckListID = empDocumentsCheckListID;
	}
	public String getDepatmentInfo() {
		return depatmentInfo;
	}
	public void setDepatmentInfo(String depatmentInfo) {
		this.depatmentInfo = depatmentInfo;
	}
	public String getCheckListClearance() {
		return checkListClearance;
	}
	public void setCheckListClearance(String checkListClearance) {
		this.checkListClearance = checkListClearance;
	}
	
	
}
