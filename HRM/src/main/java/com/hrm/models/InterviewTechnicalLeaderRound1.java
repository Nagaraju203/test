package com.hrm.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class InterviewTechnicalLeaderRound1 {
	
	@Id
	@GeneratedValue
	private Integer round1ID;
	private String statusTechRound1;
	
	private Employee techInterviewerID;
	
	private Integer rating;
	
	@OneToOne
	@JoinColumn(name="cidfk")
	private Candidate candidate;
	
	/*@ManyToMany
	@JoinTable(name="candidate_Technologies", 
	joinColumns={@JoinColumn(name=)})
	*/
	
	private List<Technologies> listOfTechnologies;

	public Integer getRound1ID() {
		return round1ID;
	}

	public void setRound1ID(Integer round1id) {
		round1ID = round1id;
	}

	public String getStatusTechRound1() {
		return statusTechRound1;
	}

	public void setStatusTechRound1(String statusTechRound1) {
		this.statusTechRound1 = statusTechRound1;
	}

	public Employee getTechInterviewerID() {
		return techInterviewerID;
	}

	public void setTechInterviewerID(Employee techInterviewerID) {
		this.techInterviewerID = techInterviewerID;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	public List<Technologies> getListOfTechnologies() {
		return listOfTechnologies;
	}

	public void setListOfTechnologies(List<Technologies> listOfTechnologies) {
		this.listOfTechnologies = listOfTechnologies;
	}

	@Override
	public String toString() {
		return "InterviewTechnicalLeaderRound1 [round1ID=" + round1ID + ", statusTechRound1=" + statusTechRound1
				+ ", techInterviewerID=" + techInterviewerID + ", rating=" + rating + ", candidate=" + candidate
				+ ", listOfTechnologies=" + listOfTechnologies + "]";
	}
	
}
