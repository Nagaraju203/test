package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class SelfApprailsal {
	@Id
	@GeneratedValue
	private Integer selfAppID;
	
	private Employee employeeID;
	
	private Department depatmentID;
	private Integer finalcialYear;
	
	private String performance;
	
	private Integer selfRating;

	public Integer getSelfAppID() {
		return selfAppID;
	}

	public void setSelfAppID(Integer selfAppID) {
		this.selfAppID = selfAppID;
	}

	public Employee getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(Employee employeeID) {
		this.employeeID = employeeID;
	}

	public Department getDepatmentID() {
		return depatmentID;
	}

	public void setDepatmentID(Department depatmentID) {
		this.depatmentID = depatmentID;
	}

	public Integer getFinalcialYear() {
		return finalcialYear;
	}

	public void setFinalcialYear(Integer finalcialYear) {
		this.finalcialYear = finalcialYear;
	}

	public String getPerformance() {
		return performance;
	}

	public void setPerformance(String performance) {
		this.performance = performance;
	}

	public Integer getSelfRating() {
		return selfRating;
	}

	public void setSelfRating(Integer selfRating) {
		this.selfRating = selfRating;
	}
	
	

}
