package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Employee {
	
	@Id
	@GeneratedValue
	private Integer empID;
	private String employeeName;
	private String employeeCode;
	private String profilePhotoName;
	private String joiningDate;
	private Department department;
	private Designation designation;
	private String empEmail;
	
	
	@OneToOne
	
	private EmployeePersonalDetails employeePersonalDetails;
	
	@OneToOne
	@JoinColumn(name="bankaccount_fk_id")
	private BankAccount bankDetails;
	@OneToOne
	@JoinColumn(name="insuranceid_fk")
	private Insurance insuranceDetails;
	@OneToOne
	@JoinColumn(name="empqual_fkid")
	private Qualification qualification;
	
	private Employee reportingManager;
	
	private String pfNumber;
	
	@OneToOne
	private Candidate candidateID;
	@OneToOne
	@JoinColumn(name="project_fk_id")
	private Projects projects;
	
	

	@OneToOne(mappedBy="employee")
	private ResignationInfo resignationInfo;
	
	@OneToOne(mappedBy="employee")
	private FinalSettlement finalSettlement;
	
	@OneToOne(mappedBy="employee")
	private NoObjectionCerificate noc;
	
	@OneToOne(mappedBy="employee")
	private EmployeeDocumentationCheckList employeeDocumentCheckList;
	

	public ResignationInfo getResignationInfo() {
		return resignationInfo;
	}

	public void setResignationInfo(ResignationInfo resignationInfo) {
		this.resignationInfo = resignationInfo;
	}

	public FinalSettlement getFinalSettlement() {
		return finalSettlement;
	}

	public void setFinalSettlement(FinalSettlement finalSettlement) {
		this.finalSettlement = finalSettlement;
	}

	public NoObjectionCerificate getNoc() {
		return noc;
	}

	public void setNoc(NoObjectionCerificate noc) {
		this.noc = noc;
	}

	public EmployeeDocumentationCheckList getEmployeeDocumentCheckList() {
		return employeeDocumentCheckList;
	}

	public void setEmployeeDocumentCheckList(EmployeeDocumentationCheckList employeeDocumentCheckList) {
		this.employeeDocumentCheckList = employeeDocumentCheckList;
	}

	
	
	
	public Integer getEmpID() {
		return empID;
	}

	public void setEmpID(Integer empID) {
		this.empID = empID;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getProfilePhotoName() {
		return profilePhotoName;
	}

	public void setProfilePhotoName(String profilePhotoName) {
		this.profilePhotoName = profilePhotoName;
	}

	public String getJoiningDate() {
		return joiningDate;
	}

	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Designation getDesignation() {
		return designation;
	}

	public void setDesignation(Designation designation) {
		this.designation = designation;
	}

	public String getEmpEmail() {
		return empEmail;
	}

	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}

	public EmployeePersonalDetails getEmployeePersonalDetails() {
		return employeePersonalDetails;
	}

	public void setEmployeePersonalDetails(EmployeePersonalDetails employeePersonalDetails) {
		this.employeePersonalDetails = employeePersonalDetails;
	}

	public BankAccount getBankDetails() {
		return bankDetails;
	}

	public void setBankDetails(BankAccount bankDetails) {
		this.bankDetails = bankDetails;
	}

	public Insurance getInsuranceDetails() {
		return insuranceDetails;
	}

	public void setInsuranceDetails(Insurance insuranceDetails) {
		this.insuranceDetails = insuranceDetails;
	}

	public Qualification getQualification() {
		return qualification;
	}

	public void setQualification(Qualification qualification) {
		this.qualification = qualification;
	}

	public Employee getReportingManager() {
		return reportingManager;
	}

	public void setReportingManager(Employee reportingManager) {
		this.reportingManager = reportingManager;
	}

	public String getPfNumber() {
		return pfNumber;
	}

	public void setPfNumber(String pfNumber) {
		this.pfNumber = pfNumber;
	}

	public Candidate getCandidateID() {
		return candidateID;
	}

	public void setCandidateID(Candidate candidateID) {
		this.candidateID = candidateID;
	}

	public Projects getProjects() {
		return projects;
	}

	public void setProjects(Projects projects) {
		this.projects = projects;
	}

	
	@Override
	public String toString() {
		return "Employee [empID=" + empID + ", employeeName=" + employeeName + ", employeeCode=" + employeeCode
				+ ", profilePhotoName=" + profilePhotoName + ", joiningDate=" + joiningDate + ", department="
				+ department + ", designation=" + designation + ", empEmail=" + empEmail + ", employeePersonalDetails="
				+ employeePersonalDetails + ", bankDetails=" + bankDetails + ", insuranceDetails=" + insuranceDetails
				+ ", qualification=" + qualification + ", reportingManager=" + reportingManager + ", pfNumber="
				+ pfNumber + ", candidateID=" + candidateID + ", projects=" + projects + "]";
	}
	
	
	
	
	
	

}
