package com.hrm.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class NoObjectionCerificate {
	@Id
	@GeneratedValue
	private Integer nocID;
	
	private List<Department> listOfDepartment;
	
	private String status;
	
	@OneToOne
	@JoinColumn(name="empid_fk")
	private Employee employee;
	
	
	@OneToOne
	@JoinColumn(name="resignationid_fk")
	private ResignationInfo resigInfo;
	
	@OneToOne
	@JoinColumn(name="finalSettlementID_fk")
	private FinalSettlement finalSettlement;
	
	@OneToOne
	@JoinColumn(name="empDocumentsCheckListID_FK")
	private EmployeeDocumentationCheckList empDocumCheckList;
	
	
	
	
	
	
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public ResignationInfo getResigInfo() {
		return resigInfo;
	}
	public void setResigInfo(ResignationInfo resigInfo) {
		this.resigInfo = resigInfo;
	}
	public FinalSettlement getFinalSettlement() {
		return finalSettlement;
	}
	public void setFinalSettlement(FinalSettlement finalSettlement) {
		this.finalSettlement = finalSettlement;
	}
	public EmployeeDocumentationCheckList getEmpDocumCheckList() {
		return empDocumCheckList;
	}
	public void setEmpDocumCheckList(EmployeeDocumentationCheckList empDocumCheckList) {
		this.empDocumCheckList = empDocumCheckList;
	}
	public Integer getNocID() {
		return nocID;
	}
	public void setNocID(Integer nocID) {
		this.nocID = nocID;
	}
	public List<Department> getListOfDepartment() {
		return listOfDepartment;
	}
	public void setListOfDepartment(List<Department> listOfDepartment) {
		this.listOfDepartment = listOfDepartment;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
