package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class InterviewProcess {
	@Id
	@GeneratedValue
	private Integer processID;
	
	private String startDate;
	private String endDate;
	
	private InterviewProcessStatus status;

	public Integer getProcessID() {
		return processID;
	}

	public void setProcessID(Integer processID) {
		this.processID = processID;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public InterviewProcessStatus getStatus() {
		return status;
	}

	public void setStatus(InterviewProcessStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "InterviewProcess [processID=" + processID + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", status=" + status + "]";
	}

	
}
