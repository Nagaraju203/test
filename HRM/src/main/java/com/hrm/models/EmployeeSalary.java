package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class EmployeeSalary {
	@Id
	@GeneratedValue
	private Integer eSalID;
	private Employee employee;
	private String month;
	private Integer year;
	private Long empGross;
	private Long empNet;
	private Integer workingDays;
	private Integer lossOfPay;
	public Integer geteSalID() {
		return eSalID;
	}
	public void seteSalID(Integer eSalID) {
		this.eSalID = eSalID;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Long getEmpGross() {
		return empGross;
	}
	public void setEmpGross(Long empGross) {
		this.empGross = empGross;
	}
	public Long getEmpNet() {
		return empNet;
	}
	public void setEmpNet(Long empNet) {
		this.empNet = empNet;
	}
	public Integer getWorkingDays() {
		return workingDays;
	}
	public void setWorkingDays(Integer workingDays) {
		this.workingDays = workingDays;
	}
	public Integer getLossOfPay() {
		return lossOfPay;
	}
	public void setLossOfPay(Integer lossOfPay) {
		this.lossOfPay = lossOfPay;
	}
	@Override
	public String toString() {
		return "EmployeeSalary [eSalID=" + eSalID + ", employee=" + employee + ", month=" + month + ", year=" + year
				+ ", empGross=" + empGross + ", empNet=" + empNet + ", workingDays=" + workingDays + ", lossOfPay="
				+ lossOfPay + "]";
	}
	
	

}
