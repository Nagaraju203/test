package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Designation {
	
	@Id
	@GeneratedValue
	private Integer designationID;
	private String designationName;
	
	
	public Integer getDesignationID() {
		return designationID;
	}
	public void setDesignationID(Integer designationID) {
		this.designationID = designationID;
	}
	public String getDesignationName() {
		return designationName;
	}
	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}
	
	@Override
	public String toString() {
		return "Designation [designationID=" + designationID + ", designationName=" + designationName + "]";
	}
	
	
	

}
