package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Experience {
	@Id
	@GeneratedValue
	private Integer expID;
	private String previousOrganization;
	private Integer yearOfExperienceInMoths;
	private Double expectedCTC;
	
	@OneToOne
	@JoinColumn(name="cidfk")
	private Candidate candidate;
	
	

	public Integer getExpID() {
		return expID;
	}

	public void setExpID(Integer expID) {
		this.expID = expID;
	}

	public String getPreviousOrganization() {
		return previousOrganization;
	}

	public void setPreviousOrganization(String previousOrganization) {
		this.previousOrganization = previousOrganization;
	}

	public Integer getYearOfExperienceInMoths() {
		return yearOfExperienceInMoths;
	}

	public void setYearOfExperienceInMoths(Integer yearOfExperienceInMoths) {
		this.yearOfExperienceInMoths = yearOfExperienceInMoths;
	}

	public Double getExpectedCTC() {
		return expectedCTC;
	}

	public void setExpectedCTC(Double expectedCTC) {
		this.expectedCTC = expectedCTC;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	@Override
	public String toString() {
		return "Experience [expID=" + expID + ", previousOrganization=" + previousOrganization
				+ ", yearOfExperienceInMoths=" + yearOfExperienceInMoths + ", expectedCTC=" + expectedCTC
				+ ", candidate=" + candidate + "]";
	}
	
	

}
