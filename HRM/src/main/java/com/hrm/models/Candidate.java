package com.hrm.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

@Entity
public class Candidate {
	
	@Id
	@GeneratedValue
	private Integer candidateID;
	private String candidateName;
	private String emailID;
	private Long phoneNumber;
	private String status;
	private String relocate;
	private Integer noticePeriodDays;	
	private String resume;
	
	@OneToOne(mappedBy="candidate")
	private OfferLetter offerLetter;
	
	@ManyToMany
	@JoinTable(name="candidate_Technologies", 
	joinColumns={@JoinColumn(name="fkcid")},
	inverseJoinColumns= {@JoinColumn(name="fktid")})

	private List<Technologies> listOfTechnologies;
	
	@OneToOne(mappedBy="candidate")
	private InterviewTechnicalLeaderRound1 interviewTechRound1;
	
	
	@OneToOne(mappedBy="candidate")
	private Experience experience;
	
	
	public Experience getExperience() {
		return experience;
	}
	public void setExperience(Experience experience) {
		this.experience = experience;
	}


	@OneToOne(mappedBy="candidate")
	private Fresher fresher;
	
	
	
	public Fresher getFresher() {
		return fresher;
	}
	public void setFresher(Fresher fresher) {
		this.fresher = fresher;
	}
	public Integer getCandidateID() {
		return candidateID;
	}
	public void setCandidateID(Integer candidateID) {
		this.candidateID = candidateID;
	}
	public String getCandidateName() {
		return candidateName;
	}
	public void setCandidateName(String candidateName) {
		this.candidateName = candidateName;
	}
	public String getEmailID() {
		return emailID;
	}
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}
	public Long getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(Long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRelocate() {
		return relocate;
	}
	public void setRelocate(String relocate) {
		this.relocate = relocate;
	}
	public Integer getNoticePeriodDays() {
		return noticePeriodDays;
	}
	public void setNoticePeriodDays(Integer noticePeriodDays) {
		this.noticePeriodDays = noticePeriodDays;
	}
	public String getResume() {
		return resume;
	}
	public void setResume(String resume) {
		this.resume = resume;
	}
	
	
	@Override
	public String toString() {
		return "Candidate [candidateID=" + candidateID + ", candidateName=" + candidateName + ", emailID=" + emailID
				+ ", phoneNumber=" + phoneNumber + ", status=" + status + ", relocate=" + relocate
				+ ", noticePeriodDays=" + noticePeriodDays + ", resume=" + resume + "]";
	}
	
	
	
	
	

}
