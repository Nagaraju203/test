package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class InterviewHR {
	@Id
	@GeneratedValue
	private Integer hrFinalRoundID;
	private Employee employeeID;
	private Integer hrRating;
	private String comments;
	
	private Candidate candidate;

	public Integer getHrFinalRoundID() {
		return hrFinalRoundID;
	}

	public void setHrFinalRoundID(Integer hrFinalRoundID) {
		this.hrFinalRoundID = hrFinalRoundID;
	}

	public Employee getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(Employee employeeID) {
		this.employeeID = employeeID;
	}

	public Integer getHrRating() {
		return hrRating;
	}

	public void setHrRating(Integer hrRating) {
		this.hrRating = hrRating;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	@Override
	public String toString() {
		return "InterviewHR [hrFinalRoundID=" + hrFinalRoundID + ", employeeID=" + employeeID + ", hrRating=" + hrRating
				+ ", comments=" + comments + ", candidate=" + candidate + "]";
	}
	
	
	

}
