package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Insurance {
	@Id
	@GeneratedValue
	private Integer insuranceID;
	private String insuranceNumber;
	private String insuranceType;	
	private String fromDate;
	private String toDate;
	
	
	
	@OneToOne(mappedBy="insuranceDetails")
	private Employee employee;
	
	
	
	public Integer getInsuranceID() {
		return insuranceID;
	}
	public void setInsuranceID(Integer insuranceID) {
		this.insuranceID = insuranceID;
	}
	public String getInsuranceNumber() {
		return insuranceNumber;
	}
	public void setInsuranceNumber(String insuranceNumber) {
		this.insuranceNumber = insuranceNumber;
	}
	public String getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	@Override
	public String toString() {
		return "Insurance [insuranceID=" + insuranceID + ", insuranceNumber=" + insuranceNumber + ", insuranceType="
				+ insuranceType + ", fromDate=" + fromDate + ", toDate=" + toDate + "]";
	}
	
	

}
