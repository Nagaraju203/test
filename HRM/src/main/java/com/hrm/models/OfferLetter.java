package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class OfferLetter {
	@Id
	@GeneratedValue
	private Integer offerID;
	private String status;
	private String offerScan;
	
	
	
	@OneToOne
	@JoinColumn(name="candidate_id_fk")
	private Candidate candidate;
	
	  Integer getOfferID() {
		return offerID;
	}
	public void setOfferID(Integer offerID) {
		this.offerID = offerID;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getOfferScan() {
		return offerScan;
	}
	public void setOfferScan(String offerScan) {
		this.offerScan = offerScan;
	}
	

}
